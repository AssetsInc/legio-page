#Legio Draconis Homepage

* based on [FuelPHP](http://www.fuelphp.com)
* Modular design

A  live demo of the state of this branch can be seen [here](http://legiodraconis.no-ip.org/). Be aware that it is under development, so things may break at random, or not be implemented yet.



## Requirements


###What is needed?

+ Linux,Unix,Windows, OSX
* a Web Server (Apache, IIS and nginx are officially supported by Fuel)
* PHP >= 5.3.3
* a SQL Database
* some general understanding of programming


### What has this been developed on?

This has been developed and tested against:

* Ubuntu Server 14.04
* Apache 2.4.9 (mod_rewrite enabled)
* PHP 5.5.14
* MariaDB 10.0.12
* [FuelPHP] 1.8/develop



## Features

* Authentication/Authorization
* a blog
* user management through a small admin backend



## Installation


###How do I use this?

Easy enough, if you know what you do. Clone the repo, point your docroot into the public directory.



## Anything else to say?


Oh, yes, in fact I do.

I'd like to thank the people over at [FuelPHP](http://www.fuelphp.com) for doing such a superb job.

They were patient enough with a newbie, who just started PHP and Fuel, being helpful and generally a nice bunch on their [IRC](irc://chat.freenode.net/fuelphp/) channel.
If you want to start with developing on [FuelPHP](http://www.fuelphp.com) yourself, drop by there and on their webpage.
