<?php
use Fuel\Validation\AbstractRule;

class unique extends AbstractRule
{
    protected $message = '{label} already exists.';
    public function validate($val, $field=null, $all=null)
    {
        list($table, $field) = explode('.', $params);

        $result = DB::select(DB::expr("LOWER (\"$field\")"))
            ->where($field, '=', Str::lower($val))
            ->from($table)->execute();

        return ! ($result->count() > 0);
    }

    public function setParameter($params)
    {
        if (is_array($params))
        {
            array_shift($params);
        }
        return parent::setParameter($params);
    }
}