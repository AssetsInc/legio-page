<?php

class Authed extends \Basic
{
    public function before()
    {
        parent::before();
        if(!\Auth::check())
        {
            \Messages::error(\Lang::get('user.errors.nologin'));
            \Response::redirect('/users/login');
        }
    }
}