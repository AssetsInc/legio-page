<?php

class Basic extends \Controller
{
    public function before()
    {
        parent::before();
        $this->data['brand'] = \Config::get('brand');
        $this->data['fuel_version'] = \Fuel::VERSION;
        $this->data['login'] = \Auth::check() ? 'true' : 'false';
        if ($this->data['login'] == "true")
        {
            list($driver,$uid) = \Auth::get_user_id();
            $user = \Model\Auth_User::query()->related('group')->where('id',$uid)->get_one();
            $this->data['user']['name'] = $user->username;
            $this->data['user']['group'] = $user->group->name;
        }
        \Log::Debug('[Basic] Messages '. \Messages::any());
        $this->data['msg'] = array();
        if (\Messages::any())
        {
            $types = array('success','info','warning','error');
            foreach ($types as $type) {
                $this->data['msg'][$type] = array();
                $msgs = Arr::pluck(\Messages::instance()->get($type),'body');
                foreach ($msgs as $msg)
                {
                    \Log::debug('[Basic] '.$type.' message:' .($msg));
                    $this->data['msg'][$type][] = $msg;
                }
            }

        }
        \Messages::reset();
    }
}