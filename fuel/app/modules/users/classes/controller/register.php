<?php

namespace users;
use Fuel\Validation\Validator;
class Controller_Register extends \Basic
{
    public function before()
    {
        parent::before();
        if (\Auth::check())
        {
            \Messages::error(\Lang::get('users.errors.loggedin'));
            \Response::redirect('/welcome/index');
        }
    }

    public function get_index()
    {
        return \Response::forge(\View::forge('register/register.twig',$this->data));
    }

    public function post_index()
    {
        $val = new Validator;
        $val->addCustomRule('unique', '\Unique');
        $val->addCustomRule('alnum','\alnum');
        $val->addField('username','Username')->required()->unique('users.username')->alnum();
        $val->addField('password','Password')->required()->minLength(6)->setMessage('The password does not meet the minimun length requirement.');
        $val->addField('pword_rep','Repeat Password')->required()->matchField('password')->setMessage('Passwords do not match');
        $val->addField('email','E-Mail')->required()->email()->unique('users.email');
        $val->addField('email_rep','Repeat E-Mail')->required()->matchField('email')->setMessage('E-Mails do not match.');
        $val->addField('accept','Accepted rules')->required()->setMessage('You did not accept the rules!');

        $reg = \Input::post();
        $res = $val->run($reg);

        if ($res->isValid())
        {
            \Log::Debug('Validation successful,creating user.');
            \Auth::create_user($reg['username'],$reg['password'],$reg['email'],1);
            \Messages::success(\Lang::get('user.registration.success'));
            \Response::redirect('/welcome/index');
        }
        else
        {
            $errors = $res->getErrors();

            foreach ($errors as $msg)
            {
                \Log::debug('Validation failed with error '.$msg);
                \Messages::error($msg);
            }
            \Messages::redirect('/users/register');
        }


    }
}