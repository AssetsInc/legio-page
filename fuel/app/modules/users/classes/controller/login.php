<?php

namespace users;

class Controller_Login extends \Basic
{

    public function before()
    {
        parent::before();
        \Config::load('user');
        if (\Auth::check())
        {
            \Messages::error(\Lang::get('You\'re already logged in.'));
            \Response::redirect_back(\Config::get('users.login.redirect'));
        }
    }

    public function get_index()
    {
        if (!empty(\Input::post()))
        {
            $this->data['extra'] = \Input::post();
        }
        return \Response::forge(\View::forge('login/login.twig',$this->data));
    }

    public function post_index()
    {
        $post = \Input::post();
        if (\Auth::validate_user($post['username'],$post['password']))
        {
            \Auth::login($post['username'],$post['password']);
            if (\Input::param('remember', false))
            {
                // create the remember-me cookie
                \Auth::remember_me();
            }
            else
            {
                // delete the remember-me cookie if present
                \Auth::dont_remember_me();
            }
            \Messages::success(\Lang::get('Login successfull'));
            \Response::redirect(\Config::get('users.login.redirect'));

        }
        else
        {
            $this->data['extra']['username'] = \Input::post('username');
            \Messages::error('Login failed. Possible username/pasword mismatch?');
            \Response::redirect('/welcome/index');
        }
    }
}