<?php
/**
 * Created by PhpStorm.
 * User: drake
 * Date: 30.06.14
 * Time: 13:10
 */

namespace users;


class Controller_Logout extends \Basic {

    public function before()
    {
        parent::before();
        if (!\Auth::check())
        {
            \Messages::error(\Lang::get('users.error.nologin'));
            \Response::redirect('welcome/index');
        }
    }

    public function get_index()
    {
        \Debug::dump(\Auth::logout());
        \Messages::success(\Lang::get('users.logout.success'));
        \Response::redirect('/welcome/index');
    }

} 