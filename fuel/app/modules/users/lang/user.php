<?php

\Log::debug('Loaded user lang file');
return array(
    'users' => array(
        'login' => array(
            'success' => 'Login successful'
        ),
        'logout' => array(
            'success' => 'Logout successful'
        ),
        'registration' => array(
            'success' => 'Registration successful'
        ),
        'errors' => array(
            'loggedin' => 'You\'re already logged in.',
            'nologin' => 'You\'re not logged in.',
            'login_failed' => 'Login failed. Possible username/pasword mismatch?'
        )
    )
);